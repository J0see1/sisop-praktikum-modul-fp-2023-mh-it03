#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <getopt.h> 

#define PORT 8080
#define COMMAND_SIZE 10000
#define FULL_COMMAND_SIZE 12002

int main(int argc, char const *argv[]) {
    char command[COMMAND_SIZE];
    char full_command[FULL_COMMAND_SIZE];
    struct sockaddr_in serv_addr;
    int sock = 0;
    char user[2000], password[2000];
    int option;
     int u_flag = 0, p_flag = 0;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\nSocket creation error\n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address/ Address not supported\n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed\n");
        return -1;
    }

    uid_t user_id = getuid();
    
    if (user_id == 0) {
        while (1) {
            printf("Enter command to send to the server: ");
            fgets(command, COMMAND_SIZE, stdin);
            
            // Remove newline character from command if present
            command[strcspn(command, "\n")] = '\0';

            // Send the command to the server
            sprintf(full_command, "\"root\" %s", command);
            printf("Full command: %s\n", full_command);

            // Send the full command to the server
            if (send(sock, full_command, strlen(full_command), 0) < 0) {
                perror("Failed to send data to server");
                exit(EXIT_FAILURE);
            }

            // // Receive and print the response from the server
            // char server_response[COMMAND_SIZE];
            // memset(server_response, 0, COMMAND_SIZE);
            // recv(sock, server_response, COMMAND_SIZE, 0);
            // printf("Server response: %s\n", server_response);
        }
    } else {
        // Receiving input for user and password
        while ((option = getopt(argc, argv, "u:p:")) != -1) {
            switch (option) {
                case 'u':
                    strncpy(user, optarg, sizeof(user) - 1);
                    u_flag = 1;
                    break;
                case 'p':
                    strncpy(password, optarg, sizeof(password) - 1);
                    p_flag = 1;
                    break;
                default:
                    printf("Invalid option\n");
                    exit(EXIT_FAILURE);
            }
        }

        // Check if both -u and -p options are provided
        if (!(u_flag && p_flag)) {
            printf("Please provide both username (-u) and password (-p)\n");
            exit(EXIT_FAILURE);
        }

        printf("User: %s, Password: %s\n", user, password);

        FILE* file = fopen("../database/databases/access/users.txt", "r");
        int userFound = 0;

        if (file == NULL) {
            // File doesn't exist, create it and set permissions
            system("../database/databases/access/users.txt");
            system("sudo chmod 777 ../database/databases/access/users.txt");
        } else {
            char line[100];
            while (fgets(line, sizeof(line), file)) {
                line[strcspn(line, "\n")] = '\0';
                if (strstr(line, user) != NULL && strstr(line, password) != NULL) {
                    userFound = 1;
                    break;
                }
            }
            fclose(file);
        }

        if (userFound == 0) {
            printf("User not found or password is incorrect\n");
            exit(EXIT_FAILURE);
        } else {
            printf("Enter command to send to the server: ");
            fgets(command, COMMAND_SIZE, stdin);
            
            // Remove newline character from command if present
            command[strcspn(command, "\n")] = '\0';

            // Send the command to the server
            sprintf(full_command, "\"%s\" %s", user, command);
            printf("Full command: %s\n", full_command);

            // Send the full command to the server
            if (send(sock, full_command, strlen(full_command), 0) < 0) {
                perror("Failed to send data to server");
                exit(EXIT_FAILURE);
            }

            // // Receive and print the response from the server
            // char server_response[COMMAND_SIZE];
            // memset(server_response, 0, COMMAND_SIZE);
            // recv(sock, server_response, COMMAND_SIZE, 0);
            // printf("Server response: %s\n", server_response);
        }
    }
    return 0;
}