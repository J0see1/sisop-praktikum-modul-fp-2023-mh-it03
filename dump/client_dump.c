#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <getopt.h>

#define PORT 8080
#define COMMAND_SIZE 10000
#define FULL_COMMAND_SIZE 12002

void saveDataToDatabase(const char *user, const char *password, const char *database) {
    // Implementasi logika penyimpanan database
    // Contoh: Menyimpan data ke file atau melakukan akses ke database sesuai kebutuhan aplikasi
    FILE *databaseFile = fopen("../database/databases/data.txt", "a");
    if (databaseFile == NULL) {
        perror("Error opening database file");
        exit(EXIT_FAILURE);
    }

    fprintf(databaseFile, "User: %s, Password: %s, Database: %s\n", user, password, database);

    fclose(databaseFile);
}

int main(int argc, char const *argv[]) {
    char user[2000], password[2000], database[2000];
    struct sockaddr_in serv_addr;
    int sock = 0;
    int option;
    int u_flag = 0, p_flag = 0, d_flag = 0;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("\nSocket creation error");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        perror("\nInvalid address/Address not supported");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("\nConnection Failed");
        return -1;
    }

    uid_t user_id = getuid();

    if (user_id == 0) {
        while (1) {
            printf("Root user detected\n");
            // Handle root user logic here
        }
    } else {
        // Receiving input for user and password
        while ((option = getopt(argc, argv, "u:p:d:")) != -1) {
            switch (option) {
                case 'u':
                    strncpy(user, optarg, sizeof(user) - 1);
                    u_flag = 1;
                    break;
                case 'p':
                    strncpy(password, optarg, sizeof(password) - 1);
                    p_flag = 1;
                    break;
                case 'd':
                    strncpy(database, optarg, sizeof(database) - 1);
                    d_flag = 1;
                    break;
                default:
                    fprintf(stderr, "Invalid option\n");
                    exit(EXIT_FAILURE);
            }
        }

        // Check if -u, -p, and -d options are provided
        if (!(u_flag && p_flag && d_flag)) {
            fprintf(stderr, "Please provide username (-u), password (-p), and database (-d)\n");
            exit(EXIT_FAILURE);
        }

        printf("User: %s, Password: %s, Database: %s\n", user, password, database);

        FILE *file = fopen("../database/databases/access/users.txt", "r");
        int userFound = 0;

        if (file == NULL) {
            // File doesn't exist, create it and set permissions
            file = fopen("../database/databases/access/users.txt", "w");
            if (file == NULL) {
                perror("Error creating users.txt");
                exit(EXIT_FAILURE);
            }
            fclose(file);

            if (chmod("../database/databases/access/users.txt", 0666) == -1) {
                perror("Error setting permissions for users.txt");
                exit(EXIT_FAILURE);
            }
        } else {
            char line[100];
            while (fgets(line, sizeof(line), file)) {
                line[strcspn(line, "\n")] = '\0';
                if (strstr(line, user) != NULL && strstr(line, password) != NULL) {
                    userFound = 1;
                    break;
                }
            }
            fclose(file);
        }

        if (userFound == 0) {
            fprintf(stderr, "User not found or password is incorrect\n");
            exit(EXIT_FAILURE);
        } else {
            printf("User found\n");
            // Handle non-root user logic here (e.g., THE USE DATABASE METHOD)
            saveDataToDatabase(user, password, database);
        }
    }
    return 0;
}