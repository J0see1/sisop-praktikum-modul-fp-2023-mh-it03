#!/bin/bash

backupDirectory="backup_files/"
runtimeDirectory="../"
timestamp=$(date +\%Y-\%m-\%d_\%H:\%M:\%S)
zipFile="$timestamp-archive.zip"

createBackup() {
    local directory="$1"
    sudo ~/backup-scripts/dump_script "$(basename "$directory")" > "$backupDirectory/$(basename "$directory").backup"
}

copyLogToBackup() {
    cp "$runtimeDirectory/log.txt" "$backupDirectory"
}

clearOriginalLog() {
    echo -n > "$runtimeDirectory/log.txt"
}

createZip() {
    cd "$backupDirectory" && zip "$zipFile" *.backup log.txt
    find "$backupDirectory" -maxdepth 1 -type f ! -name "$zipFile" -delete
}

# Create backups for each sub-directory
for directory in "$backupDirectory"/*; do
    createBackup "$directory"
done

copyLogToBackup
clearOriginalLog
createZip
