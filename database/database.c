#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdbool.h>

#define PORT 8080
#define COMMAND_SIZE 10000

void log_command(const char *username, const char *command) {
    //menggunakan time saat ini
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_info);

    FILE *file = fopen("log.txt", "a");
    if (file == NULL) {
        perror("failed opening log.txt");
        return;
    }

    //menghilangkan ";"
    char clean_command[100];
    strcpy(clean_command, command);
    char *semicolon_plc = strchr(clean_command, ';');
    if (semicolon_plc != NULL) {
        *semicolon_plc = '\0';
    }

    //memasukan file ke log.txt
    fprintf(file, "%s:%s:%s\n", timestamp, username, clean_command);

    fclose(file);
}

void grantPermission(char* user, char* databaseName) {
    FILE *file = fopen("databases/access/permission.txt", "a");
    if (file == NULL) {
        perror("Error opening permission file");
        return;
    }

    fprintf(file, "%s;%s\n", user, databaseName);
    fclose(file);
}

bool hasPermission(char* user, char* databaseName) {
    if (strcmp(user, "root") == 0) {
        return true; // Grant all permissions to the root user
    }

    FILE *file = fopen("databases/access/permission.txt", "r");
    if (file == NULL) {
        perror("Error opening permission file");
        return false;
    }

    char line[COMMAND_SIZE];
    char db[100], usr[100];
    int found = false;

    while (fgets(line, sizeof(line), file)) {
        if (sscanf(line, "%[^;];%[^\n]", usr, db) == 2) {
            if (strcmp(usr, user) == 0 && strcmp(db, databaseName) == 0) {
                found = true;
                break;
            }
        }
    }

    fclose(file);
    if(found == true) {
        return true;
    } else {
        return false;
    }
}

void useDatabase(char* user, char* databaseName) {
    if (hasPermission(user, databaseName)) {
        printf("User %s is using database %s\n", user, databaseName);
    } else {
        printf("User %s does not have permission to use database %s\n", user, databaseName);
    }
}

void addUser(char* user, char* password) {
    FILE *file = fopen("databases/access/users.txt", "a");
    if (file == NULL) {
        perror("Error opening users file");
        return;
    }

    fprintf(file, "%s;%s\n", user, password);
    fclose(file);
}

void createDatabase(char* user, char* databaseName){
    char command[COMMAND_SIZE];
    sprintf(command, "mkdir databases/%s", databaseName);

    int task = system(command);

    if (task == 1){
        printf(" Failure at creating %s database", databaseName);
    } else {
        printf("Success at creating %s database", databaseName);

        grantPermission(user, databaseName);
    }
}

void createTable(char* databaseName, char* tableName) {
    char command[COMMAND_SIZE];
    sprintf(command, "touch databases/%s/%s.txt", databaseName, tableName);
    int task = system(command);

    if (task != 0) {
        printf("Failed to create %s table in %s database\n", tableName, databaseName);
    } else {
        printf("Created %s table in %s database\n", tableName, databaseName);
    }
}

void dropColumn(char* databaseName, char* tableName, char* columnName) {
    char command[COMMAND_SIZE];
    sprintf(command, "sed -i '' '/%s/d' databases/%s/%s.txt", columnName, databaseName, tableName);
    int task = system(command);

    if (task != 0) {
        printf("Failed to drop %s column from %s table in %s database\n", columnName, tableName, databaseName);
    } else {
        printf("Dropped %s column from %s table in %s database\n", columnName, tableName, databaseName);
    }
}

void dropTable(char* databaseName, char* tableName) {
    char command[COMMAND_SIZE];
    sprintf(command, "rm -rf databases/%s/%s.txt", databaseName, tableName);
    int task = system(command);

    if (task != 0) {
        printf("Failed to delete %s table from %s database\n", tableName, databaseName);
    } else {
        printf("Deleted %s table from %s database\n", tableName, databaseName);
    }
}

void dropDatabase(char* databaseName) {
    char command[COMMAND_SIZE];
    sprintf(command, "rm -rf databases/%s", databaseName);
    int task = system(command);

    if (task != 0) {
        printf("Failed to delete %s database\n", databaseName);
    } else {
        printf("Deleted %s database\n", databaseName);
    }
}

void insertData(char* databaseName, char* tableName, char* data) {
    char filePath[COMMAND_SIZE];
    sprintf(filePath, "databases/%s/%s.txt", databaseName, tableName);

    FILE *file = fopen(filePath, "a");
    if (file == NULL) {
        printf("Error opening table file\n");
        return;
    }

    fprintf(file, "%s\n", data);
    fclose(file);

    printf("Data inserted into %s table in %s database\n", tableName, databaseName);
}

void insertIntoTable(char* user, char* message) {
    char tableName[100];
    char values[COMMAND_SIZE];
    sscanf(message, "INSERT INTO %s %[^\n]", tableName, values);

    char databaseName[100]; 
    if (hasPermission(user, databaseName)) {
        insertData(databaseName, tableName, values);
    } else {
        printf("User %s does not have permission to insert into table %s in database %s\n", user, tableName, databaseName);
    }
}

void updateTable(char* user, char* message) {
    char tableName[100];
    char columnName[100];
    char value[COMMAND_SIZE];
    
    if (sscanf(message, "UPDATE %s SET %[^=]=%[^;];", tableName, columnName, value) == 3) {
        char databaseName[100]; 
        if (hasPermission(user, databaseName)) {
            // Logic to update the table
            char command[COMMAND_SIZE];
            sprintf(command, "sed -i '' 's/^%s=[^\n]*/%s=%s/' databases/%s/%s.txt", columnName, columnName, value, databaseName, tableName);
            
            int task = system(command);
            
            if (task != 0) {
                printf("Failed to update %s table in %s database\n", tableName, databaseName);
            } else {
                printf("Updated %s table in %s database\n", tableName, databaseName);
            }
        } else {
            printf("User %s does not have permission to update table %s in database %s\n", user, tableName, databaseName);
        }
    } else {
        printf("Invalid UPDATE command format\n");
    }
}

void updateWithWhere(char* user, char* message) {
    char tableName[100];
    char columnName[100];
    char value[COMMAND_SIZE];
    
    // Extracting data from the command
    sscanf(message, "UPDATE %s SET %[^=]=%[^;];", tableName, columnName, value);

    char databaseName[100]; // Define and assign the appropriate database name
    
    if (hasPermission(user, databaseName)) {
        // Open the file corresponding to the table for reading and writing
        char filePath[COMMAND_SIZE];
        sprintf(filePath, "databases/%s/%s.txt", databaseName, tableName);
        FILE* file = fopen(filePath, "r+");
        
        if (file == NULL) {
            printf("Table %s does not exist in database %s\n", tableName, databaseName);
            return;
        }

        char line[COMMAND_SIZE];
        char updatedLine[COMMAND_SIZE];
        while (fgets(line, sizeof(line), file)) {
            char *token = strtok(line, ",");
            while (token != NULL) {
                if (strstr(token, columnName) != NULL) {
                    // Assuming the condition column is found, update the line
                    if (strstr(token, value) != NULL) {
                        strcpy(updatedLine, line);
                        break;
                    }
                }
                token = strtok(NULL, ",");
            }
        }

        // Move the file pointer to the line where the update is required and write the updated line
        fseek(file, -strlen(updatedLine), SEEK_CUR);
        fprintf(file, "%s", updatedLine);
        
        fclose(file);
    } else {
        printf("User %s does not have permission to update table %s in database %s\n", user, tableName, databaseName);
    }
}

void deleteFromTable(char* user, char* message) {
    char tableName[100];
    
    if (sscanf(message, "DELETE FROM %s;", tableName) == 1) {
        char databaseName[100]; 
        if (hasPermission(user, databaseName)) {
            // Logic to delete data from the table
            char command[COMMAND_SIZE];
            sprintf(command, "echo '' > databases/%s/%s.txt", databaseName, tableName);
            
            int task = system(command);
            
            if (task != 0) {
                printf("Failed to delete data from %s table in %s database\n", tableName, databaseName);
            } else {
                printf("Deleted data from %s table in %s database\n", tableName, databaseName);
            }
        } else {
            printf("User %s does not have permission to delete from table %s in database %s\n", user, tableName, databaseName);
        }
    } else {
        printf("Invalid DELETE command format\n");
    }
}

void deleteWithWhere(char* user, char* message) {
    char tableName[100];
    char columnName[100];
    char value[COMMAND_SIZE];

    // Extracting data from the command
    sscanf(message, "DELETE FROM %s WHERE %[^=]=%[^;];", tableName, columnName, value);

    char databaseName[100]; // Define and assign the appropriate database name
    
    if (hasPermission(user, databaseName)) {
        // Open the file corresponding to the table for reading and writing
        char filePath[COMMAND_SIZE];
        sprintf(filePath, "databases/%s/%s.txt", databaseName, tableName);
        FILE* file = fopen(filePath, "r+");
        
        if (file == NULL) {
            printf("Table %s does not exist in database %s\n", tableName, databaseName);
            return;
        }

        char line[COMMAND_SIZE];
        char updatedLine[COMMAND_SIZE];
        while (fgets(line, sizeof(line), file)) {
            char *token = strtok(line, ",");
            while (token != NULL) {
                if (strstr(token, columnName) != NULL && strstr(token, value) != NULL) {
                    // Assuming the condition column is found, do not write the line
                    break;
                }
                token = strtok(NULL, ",");
            }
        }

        fclose(file);
    } else {
        printf("User %s does not have permission to delete from table %s in database %s\n", user, tableName, databaseName);
    }
}

void selectFromTable(char* user, char* message) {
    char columns[COMMAND_SIZE];
    char tableName[100];
    sscanf(message, "SELECT %[^ ] FROM %s", columns, tableName);

    char databaseName[100]; // Define and assign the appropriate database name
    
    if (hasPermission(user, databaseName)) {
        // Open the file corresponding to the table for reading
        char filePath[COMMAND_SIZE];
        sprintf(filePath, "databases/%s/%s.txt", databaseName, tableName);
        FILE* file = fopen(filePath, "r");
        
        if (file == NULL) {
            printf("Table %s does not exist in database %s\n", tableName, databaseName);
            return;
        }

        // Logic for retrieving and displaying data based on the columns
        char line[COMMAND_SIZE];
        while (fgets(line, sizeof(line), file)) {
            // Assuming data is stored with comma-separated values in each row
            char *token = strtok(line, ",");
            while (token != NULL) {
                // If the requested column matches, print the value
                if (strcmp(columns, "*") == 0 || strstr(columns, token) != NULL) {
                    printf("%s ", token);
                }
                token = strtok(NULL, ",");
            }
            printf("\n");
        }

        fclose(file);
    } else {
        printf("User %s does not have permission to select from table %s in database %s\n", user, tableName, databaseName);
    }
}

void selectWithWhere(char* user, char* message) {
    char columns[COMMAND_SIZE];
    char tableName[100];
    char columnName[100];
    char value[COMMAND_SIZE];

    // Extracting data from the command
    sscanf(message, "SELECT %[^ ] FROM %s WHERE %[^=]=%[^;];", columns, tableName, columnName, value);

    char databaseName[100]; // Define and assign the appropriate database name
    
    if (hasPermission(user, databaseName)) {
        // Open the file corresponding to the table for reading
        char filePath[COMMAND_SIZE];
        sprintf(filePath, "databases/%s/%s.txt", databaseName, tableName);
        FILE* file = fopen(filePath, "r");
        
        if (file == NULL) {
            printf("Table %s does not exist in database %s\n", tableName, databaseName);
            return;
        }

        char line[COMMAND_SIZE];
        while (fgets(line, sizeof(line), file)) {
            char *token = strtok(line, ",");
            while (token != NULL) {
                if (strstr(token, columnName) != NULL && strstr(token, value) != NULL) {
                    // Assuming data is stored with comma-separated values in each row
                    // Print the entire row if the condition is met
                    printf("%s", line);
                    break;
                }
                token = strtok(NULL, ",");
            }
        }

        fclose(file);
    } else {
        printf("User %s does not have permission to select from table %s in database %s\n", user, tableName, databaseName);
    }
}

void processCommand(int new_socket, char *message) {
    char argument[COMMAND_SIZE];
    char response[COMMAND_SIZE];
    // sprintf(response, "Server received the command: %s", message);
    send(new_socket, response, strlen(response), 0);

    char user[100];
    char full_command[COMMAND_SIZE];
    
    if (sscanf(message, "\"%99[^\"]\" %9999[^\n]", user, full_command) == 2) {

    // Store full_command in a separate variable for later use
    char primary_command[100]; 
    sscanf(full_command, "%s", primary_command);
        if (strcmp(primary_command, "USE") == 0) {
            char databaseName[100];
            sscanf(full_command, "USE DATABASE %s", databaseName);
            useDatabase(user, databaseName);
        } else if (strcmp(primary_command, "CREATE") == 0) {
            char argument[100];
            sscanf(full_command, "%*s %s", argument);

            if (strcmp(argument, "USER") == 0) {
                char user[100];
                char password[100];

                sscanf(full_command, "CREATE USER %s IDENTIFIED BY %s", user, password);
                addUser(user, password);
            } else if (strcmp(argument, "DATABASE") == 0) {
                char databaseName[100];
                sscanf(full_command, "CREATE DATABASE %s", databaseName);
                createDatabase(user, databaseName);
            }  else if (strcmp(argument, "TABLE") == 0) {
                char databaseName[100];
                char tableName[100];
                
                sscanf(full_command, "CREATE TABLE %s IN %s", tableName, databaseName);
                createTable(databaseName, tableName);
            }
        } else if (strcmp(primary_command, "GRANT") == 0) {
            char argument[100];
            sscanf(full_command, "%*s %s", argument);

            if (strcmp(argument, "PERMISSION") == 0) {
                char databaseName[100];
                char user[100];

                sscanf(full_command, "GRANT PERMISSION %s INTO %s", user, databaseName);
                grantPermission(user, databaseName);
            }
        } else if (strcmp(primary_command, "DROP") == 0) {
            char argument[100];
            sscanf(full_command, "%*s %s", argument);

            if (strcmp(argument, "DATABASE") == 0) {
                char databaseName[100];
                sscanf(full_command, "DROP DATABASE %s", databaseName);
                dropDatabase(databaseName);
            } else if (strcmp(argument, "TABLE") == 0) {
                char databaseName[100];
                char tableName[100];
                sscanf(full_command, "DROP TABLE %s FROM %s", tableName, databaseName);
                dropTable(databaseName, tableName);
            } else if (strcmp(argument, "COLUMN") == 0) {
                char databaseName[100];
                char tableName[100];
                char columnName[100];
                sscanf(full_command, "DROP COLUMN %s FROM %s.%s", columnName, tableName, databaseName);
                dropColumn(databaseName, tableName, columnName);
            }
        } else if (strcmp(primary_command, "INSERT") == 0) {
            insertIntoTable(user, full_command);
        } else if (strcmp(primary_command, "UPDATE") == 0) {
            if (strcmp(argument, "WHERE") == 0) {
                updateWithWhere(user, full_command);
            } else {
            updateTable(user, full_command);
            }
        } else if (strcmp(primary_command, "DELETE") == 0) {
            if (strcmp(argument, "WHERE") == 0) {
                deleteWithWhere(user, full_command);
            } else {
            updateTable(user, full_command);
            }
        } else if (strcmp(primary_command, "SELECT") == 0) {
            if (strcmp(argument, "WHERE") == 0) {
                selectWithWhere(user, full_command);
            } else {
            updateTable(user, full_command);
            }
        } else {
            printf("Unrecognized command\n");
            }
    }
}

int main(int argc, char const *argv[]) {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char command[COMMAND_SIZE];
    char full_command[COMMAND_SIZE];
    system("mkdir databases");
    system("mkdir databases/access");
    system("touch databases/access/users.txt");
    system("touch databases/access/permission.txt");

    // Create socket file descriptor
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Set socket options
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("Setsockopt failed");
        exit(EXIT_FAILURE);
    }

    // Define socket address and port
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Bind the socket to the specified port
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    // Listen for incoming connections
    if (listen(server_fd, 3) < 0) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

        printf("Server is waiting for incoming connections...\n");

    while (1) {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
            perror("Accept failed");
            exit(EXIT_FAILURE);
        }

        printf("New connection established\n");

        // Receive data from client and process it
        while (1) {
            memset(full_command, 0, COMMAND_SIZE);
            int valread;
            if ((valread = read(new_socket, full_command, COMMAND_SIZE)) == 0) {
                printf("Connection closed by client\n");
                break;
            }

            // Process the received full_command
            printf("Command received from client: %s\n", full_command);

            // Process the message received from the client
            processCommand(new_socket, full_command);
        }

        close(new_socket);
        printf("Client connection closed\n");
    }

    close(server_fd);
    return 0;
}