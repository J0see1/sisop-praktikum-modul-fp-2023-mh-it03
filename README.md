# sisop-praktikum-modul-fp-2023-mh-it03

Pengerjaan soal shift sistem operasi modul fp oleh IT03

# Anggota

| Nama                            | NRP          |
| ------------------------------- | ------------ |
| Marcelinus Alvinanda Chrisantya | `5027221012` |
| George David Nebore             | `5027221043` |
| Angella Christie                | `5027221047` |

# client.c

```
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <getopt.h> 

#define PORT 8080
#define COMMAND_SIZE 10000
#define FULL_COMMAND_SIZE 12002

int main(int argc, char const *argv[]) {
    char command[COMMAND_SIZE];
    char full_command[FULL_COMMAND_SIZE];
    struct sockaddr_in serv_addr;
    int sock = 0;
    char user[2000], password[2000];
    int option;
     int u_flag = 0, p_flag = 0;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\nSocket creation error\n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address/ Address not supported\n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed\n");
        return -1;
    }

    uid_t user_id = getuid();
    
    if (user_id == 0) {
        while (1) {
            printf("Enter command to send to the server: ");
            fgets(command, COMMAND_SIZE, stdin);
            
            // Remove newline character from command if present
            command[strcspn(command, "\n")] = '\0';

            // Send the command to the server
            sprintf(full_command, "\"root\" %s", command);
            printf("Full command: %s\n", full_command);

            // Send the full command to the server
            if (send(sock, full_command, strlen(full_command), 0) < 0) {
                perror("Failed to send data to server");
                exit(EXIT_FAILURE);
            }

            // // Receive and print the response from the server
            // char server_response[COMMAND_SIZE];
            // memset(server_response, 0, COMMAND_SIZE);
            // recv(sock, server_response, COMMAND_SIZE, 0);
            // printf("Server response: %s\n", server_response);
        }
    } else {
        // Receiving input for user and password
        while ((option = getopt(argc, argv, "u:p:")) != -1) {
            switch (option) {
                case 'u':
                    strncpy(user, optarg, sizeof(user) - 1);
                    u_flag = 1;
                    break;
                case 'p':
                    strncpy(password, optarg, sizeof(password) - 1);
                    p_flag = 1;
                    break;
                default:
                    printf("Invalid option\n");
                    exit(EXIT_FAILURE);
            }
        }

        // Check if both -u and -p options are provided
        if (!(u_flag && p_flag)) {
            printf("Please provide both username (-u) and password (-p)\n");
            exit(EXIT_FAILURE);
        }

        printf("User: %s, Password: %s\n", user, password);

        FILE* file = fopen("../database/databases/access/users.txt", "r");
        int userFound = 0;

        if (file == NULL) {
            // File doesn't exist, create it and set permissions
            system("../database/databases/access/users.txt");
            system("sudo chmod 777 ../database/databases/access/users.txt");
        } else {
            char line[100];
            while (fgets(line, sizeof(line), file)) {
                line[strcspn(line, "\n")] = '\0';
                if (strstr(line, user) != NULL && strstr(line, password) != NULL) {
                    userFound = 1;
                    break;
                }
            }
            fclose(file);
        }

        if (userFound == 0) {
            printf("User not found or password is incorrect\n");
            exit(EXIT_FAILURE);
        } else {
            printf("Enter command to send to the server: ");
            fgets(command, COMMAND_SIZE, stdin);
            
            // Remove newline character from command if present
            command[strcspn(command, "\n")] = '\0';

            // Send the command to the server
            sprintf(full_command, "\"%s\" %s", user, command);
            printf("Full command: %s\n", full_command);

            // Send the full command to the server
            if (send(sock, full_command, strlen(full_command), 0) < 0) {
                perror("Failed to send data to server");
                exit(EXIT_FAILURE);
            }

            // // Receive and print the response from the server
            // char server_response[COMMAND_SIZE];
            // memset(server_response, 0, COMMAND_SIZE);
            // recv(sock, server_response, COMMAND_SIZE, 0);
            // printf("Server response: %s\n", server_response);
        }
    }
    return 0;
}
```
# Penjelasan Program
```
int main(int argc, char const *argv[]) {
    // ...
    int option;
    int u_flag = 0, p_flag = 0;
    // ...
}
```
- Program menggunakan `argc` dan `argv` untuk menerima argumen baris perintah. Variabel `option` digunakan untuk menyimpan nilai opsi yang diproses oleh `getopt`. Variabel `u_flag` dan `p_flag` digunakan untuk melacak apakah opsi `-u` dan `-p` telah diberikan oleh pengguna.
```
while ((option = getopt(argc, argv, "u:p:")) != -1) {
    switch (option) {
        case 'u':
            strncpy(user, optarg, sizeof(user) - 1);
            u_flag = 1;
            break;
        case 'p':
            strncpy(password, optarg, sizeof(password) - 1);
            p_flag = 1;
            break;
        default:
            printf("Invalid option\n");
            exit(EXIT_FAILURE);
    }
}
```
- Program menggunakan fungsi `getopt` untuk memproses opsi baris perintah. Dalam hal ini, program mendukung opsi `-u` untuk menyediakan nama pengguna (username) dan opsi `-p` untuk menyediakan kata sandi (password). Jika opsi yang tidak dikenali diberikan, program menampilkan pesan kesalahan dan keluar.
```
if (user_id == 0) {
    while (1) {
        // ...
        sprintf(full_command, "\"root\" %s", command);
        // ...
    }
}
```
- Jika program dijalankan sebagai root (dengan ID pengguna 0), program akan meminta pengguna untuk memasukkan perintah. Setiap perintah akan dikirimkan dengan menambahkan identifikasi "root". Ini mengasumsikan
bahwa perintah yang dimasukkan oleh root memiliki format khusus dengan identifikasi "root".
```
if (!(u_flag && p_flag)) {
            printf("Please provide both username (-u) and password (-p)\n");
            exit(EXIT_FAILURE);
        }
```
- Program memeriksa apakah opsi `-u` dan `-p` telah diberikan. Jika tidak, program menampilkan pesan kesalahan dan keluar.

# database.c 
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdbool.h>

#define PORT 8080
#define COMMAND_SIZE 10000

void log_command(const char *username, const char *command) {
    //menggunakan time saat ini
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_info);

    FILE *file = fopen("log.txt", "a");
    if (file == NULL) {
        perror("failed opening log.txt");
        return;
    }

    //menghilangkan ";"
    char clean_command[100];
    strcpy(clean_command, command);
    char *semicolon_plc = strchr(clean_command, ';');
    if (semicolon_plc != NULL) {
        *semicolon_plc = '\0';
    }

    //memasukan file ke log.txt
    fprintf(file, "%s:%s:%s\n", timestamp, username, clean_command);

    fclose(file);
}

void grantPermission(char* user, char* databaseName) {
    FILE *file = fopen("databases/access/permission.txt", "a");
    if (file == NULL) {
        perror("Error opening permission file");
        return;
    }

    fprintf(file, "%s;%s\n", user, databaseName);
    fclose(file);
}

bool hasPermission(char* user, char* databaseName) {
    if (strcmp(user, "root") == 0) {
        return true; // Grant all permissions to the root user
    }

    FILE *file = fopen("databases/access/permission.txt", "r");
    if (file == NULL) {
        perror("Error opening permission file");
        return false;
    }

    char line[COMMAND_SIZE];
    char db[100], usr[100];
    int found = false;

    while (fgets(line, sizeof(line), file)) {
        if (sscanf(line, "%[^;];%[^\n]", usr, db) == 2) {
            if (strcmp(usr, user) == 0 && strcmp(db, databaseName) == 0) {
                found = true;
                break;
            }
        }
    }

    fclose(file);
    if(found == true) {
        return true;
    } else {
        return false;
    }
}

void useDatabase(char* user, char* databaseName) {
    if (hasPermission(user, databaseName)) {
        printf("User %s is using database %s\n", user, databaseName);
    } else {
        printf("User %s does not have permission to use database %s\n", user, databaseName);
    }
}

void addUser(char* user, char* password) {
    FILE *file = fopen("databases/access/users.txt", "a");
    if (file == NULL) {
        perror("Error opening users file");
        return;
    }

    fprintf(file, "%s;%s\n", user, password);
    fclose(file);
}

void createDatabase(char* user, char* databaseName){
    char command[COMMAND_SIZE];
    sprintf(command, "mkdir databases/%s", databaseName);

    int task = system(command);

    if (task == 1){
        printf(" Failure at creating %s database", databaseName);
    } else {
        printf("Success at creating %s database", databaseName);

        grantPermission(user, databaseName);
    }
}

void createTable(char* databaseName, char* tableName) {
    char command[COMMAND_SIZE];
    sprintf(command, "touch databases/%s/%s.txt", databaseName, tableName);
    int task = system(command);

    if (task != 0) {
        printf("Failed to create %s table in %s database\n", tableName, databaseName);
    } else {
        printf("Created %s table in %s database\n", tableName, databaseName);
    }
}

void dropColumn(char* databaseName, char* tableName, char* columnName) {
    char command[COMMAND_SIZE];
    sprintf(command, "sed -i '' '/%s/d' databases/%s/%s.txt", columnName, databaseName, tableName);
    int task = system(command);

    if (task != 0) {
        printf("Failed to drop %s column from %s table in %s database\n", columnName, tableName, databaseName);
    } else {
        printf("Dropped %s column from %s table in %s database\n", columnName, tableName, databaseName);
    }
}

void dropTable(char* databaseName, char* tableName) {
    char command[COMMAND_SIZE];
    sprintf(command, "rm -rf databases/%s/%s.txt", databaseName, tableName);
    int task = system(command);

    if (task != 0) {
        printf("Failed to delete %s table from %s database\n", tableName, databaseName);
    } else {
        printf("Deleted %s table from %s database\n", tableName, databaseName);
    }
}

void dropDatabase(char* databaseName) {
    char command[COMMAND_SIZE];
    sprintf(command, "rm -rf databases/%s", databaseName);
    int task = system(command);

    if (task != 0) {
        printf("Failed to delete %s database\n", databaseName);
    } else {
        printf("Deleted %s database\n", databaseName);
    }
}

void insertData(char* databaseName, char* tableName, char* data) {
    char filePath[COMMAND_SIZE];
    sprintf(filePath, "databases/%s/%s.txt", databaseName, tableName);

    FILE *file = fopen(filePath, "a");
    if (file == NULL) {
        printf("Error opening table file\n");
        return;
    }

    fprintf(file, "%s\n", data);
    fclose(file);

    printf("Data inserted into %s table in %s database\n", tableName, databaseName);
}

void insertIntoTable(char* user, char* message) {
    char tableName[100];
    char values[COMMAND_SIZE];
    sscanf(message, "INSERT INTO %s %[^\n]", tableName, values);

    char databaseName[100]; 
    if (hasPermission(user, databaseName)) {
        insertData(databaseName, tableName, values);
    } else {
        printf("User %s does not have permission to insert into table %s in database %s\n", user, tableName, databaseName);
    }
}

void updateTable(char* user, char* message) {
    char tableName[100];
    char columnName[100];
    char value[COMMAND_SIZE];
    
    if (sscanf(message, "UPDATE %s SET %[^=]=%[^;];", tableName, columnName, value) == 3) {
        char databaseName[100]; 
        if (hasPermission(user, databaseName)) {
            // Logic to update the table
            char command[COMMAND_SIZE];
            sprintf(command, "sed -i '' 's/^%s=[^\n]*/%s=%s/' databases/%s/%s.txt", columnName, columnName, value, databaseName, tableName);
            
            int task = system(command);
            
            if (task != 0) {
                printf("Failed to update %s table in %s database\n", tableName, databaseName);
            } else {
                printf("Updated %s table in %s database\n", tableName, databaseName);
            }
        } else {
            printf("User %s does not have permission to update table %s in database %s\n", user, tableName, databaseName);
        }
    } else {
        printf("Invalid UPDATE command format\n");
    }
}

void updateWithWhere(char* user, char* message) {
    char tableName[100];
    char columnName[100];
    char value[COMMAND_SIZE];
    
    // Extracting data from the command
    sscanf(message, "UPDATE %s SET %[^=]=%[^;];", tableName, columnName, value);

    char databaseName[100]; // Define and assign the appropriate database name
    
    if (hasPermission(user, databaseName)) {
        // Open the file corresponding to the table for reading and writing
        char filePath[COMMAND_SIZE];
        sprintf(filePath, "databases/%s/%s.txt", databaseName, tableName);
        FILE* file = fopen(filePath, "r+");
        
        if (file == NULL) {
            printf("Table %s does not exist in database %s\n", tableName, databaseName);
            return;
        }

        char line[COMMAND_SIZE];
        char updatedLine[COMMAND_SIZE];
        while (fgets(line, sizeof(line), file)) {
            char *token = strtok(line, ",");
            while (token != NULL) {
                if (strstr(token, columnName) != NULL) {
                    // Assuming the condition column is found, update the line
                    if (strstr(token, value) != NULL) {
                        strcpy(updatedLine, line);
                        break;
                    }
                }
                token = strtok(NULL, ",");
            }
        }

        // Move the file pointer to the line where the update is required and write the updated line
        fseek(file, -strlen(updatedLine), SEEK_CUR);
        fprintf(file, "%s", updatedLine);
        
        fclose(file);
    } else {
        printf("User %s does not have permission to update table %s in database %s\n", user, tableName, databaseName);
    }
}

void deleteFromTable(char* user, char* message) {
    char tableName[100];
    
    if (sscanf(message, "DELETE FROM %s;", tableName) == 1) {
        char databaseName[100]; 
        if (hasPermission(user, databaseName)) {
            // Logic to delete data from the table
            char command[COMMAND_SIZE];
            sprintf(command, "echo '' > databases/%s/%s.txt", databaseName, tableName);
            
            int task = system(command);
            
            if (task != 0) {
                printf("Failed to delete data from %s table in %s database\n", tableName, databaseName);
            } else {
                printf("Deleted data from %s table in %s database\n", tableName, databaseName);
            }
        } else {
            printf("User %s does not have permission to delete from table %s in database %s\n", user, tableName, databaseName);
        }
    } else {
        printf("Invalid DELETE command format\n");
    }
}

void deleteWithWhere(char* user, char* message) {
    char tableName[100];
    char columnName[100];
    char value[COMMAND_SIZE];

    // Extracting data from the command
    sscanf(message, "DELETE FROM %s WHERE %[^=]=%[^;];", tableName, columnName, value);

    char databaseName[100]; // Define and assign the appropriate database name
    
    if (hasPermission(user, databaseName)) {
        // Open the file corresponding to the table for reading and writing
        char filePath[COMMAND_SIZE];
        sprintf(filePath, "databases/%s/%s.txt", databaseName, tableName);
        FILE* file = fopen(filePath, "r+");
        
        if (file == NULL) {
            printf("Table %s does not exist in database %s\n", tableName, databaseName);
            return;
        }

        char line[COMMAND_SIZE];
        char updatedLine[COMMAND_SIZE];
        while (fgets(line, sizeof(line), file)) {
            char *token = strtok(line, ",");
            while (token != NULL) {
                if (strstr(token, columnName) != NULL && strstr(token, value) != NULL) {
                    // Assuming the condition column is found, do not write the line
                    break;
                }
                token = strtok(NULL, ",");
            }
        }

        fclose(file);
    } else {
        printf("User %s does not have permission to delete from table %s in database %s\n", user, tableName, databaseName);
    }
}

void selectFromTable(char* user, char* message) {
    char columns[COMMAND_SIZE];
    char tableName[100];
    sscanf(message, "SELECT %[^ ] FROM %s", columns, tableName);

    char databaseName[100]; // Define and assign the appropriate database name
    
    if (hasPermission(user, databaseName)) {
        // Open the file corresponding to the table for reading
        char filePath[COMMAND_SIZE];
        sprintf(filePath, "databases/%s/%s.txt", databaseName, tableName);
        FILE* file = fopen(filePath, "r");
        
        if (file == NULL) {
            printf("Table %s does not exist in database %s\n", tableName, databaseName);
            return;
        }

        // Logic for retrieving and displaying data based on the columns
        char line[COMMAND_SIZE];
        while (fgets(line, sizeof(line), file)) {
            // Assuming data is stored with comma-separated values in each row
            char *token = strtok(line, ",");
            while (token != NULL) {
                // If the requested column matches, print the value
                if (strcmp(columns, "*") == 0 || strstr(columns, token) != NULL) {
                    printf("%s ", token);
                }
                token = strtok(NULL, ",");
            }
            printf("\n");
        }

        fclose(file);
    } else {
        printf("User %s does not have permission to select from table %s in database %s\n", user, tableName, databaseName);
    }
}

void selectWithWhere(char* user, char* message) {
    char columns[COMMAND_SIZE];
    char tableName[100];
    char columnName[100];
    char value[COMMAND_SIZE];

    // Extracting data from the command
    sscanf(message, "SELECT %[^ ] FROM %s WHERE %[^=]=%[^;];", columns, tableName, columnName, value);

    char databaseName[100]; // Define and assign the appropriate database name
    
    if (hasPermission(user, databaseName)) {
        // Open the file corresponding to the table for reading
        char filePath[COMMAND_SIZE];
        sprintf(filePath, "databases/%s/%s.txt", databaseName, tableName);
        FILE* file = fopen(filePath, "r");
        
        if (file == NULL) {
            printf("Table %s does not exist in database %s\n", tableName, databaseName);
            return;
        }

        char line[COMMAND_SIZE];
        while (fgets(line, sizeof(line), file)) {
            char *token = strtok(line, ",");
            while (token != NULL) {
                if (strstr(token, columnName) != NULL && strstr(token, value) != NULL) {
                    // Assuming data is stored with comma-separated values in each row
                    // Print the entire row if the condition is met
                    printf("%s", line);
                    break;
                }
                token = strtok(NULL, ",");
            }
        }

        fclose(file);
    } else {
        printf("User %s does not have permission to select from table %s in database %s\n", user, tableName, databaseName);
    }
}

void processCommand(int new_socket, char *message) {
    char argument[COMMAND_SIZE];
    char response[COMMAND_SIZE];
    // sprintf(response, "Server received the command: %s", message);
    send(new_socket, response, strlen(response), 0);

    char user[100];
    char full_command[COMMAND_SIZE];
    
    if (sscanf(message, "\"%99[^\"]\" %9999[^\n]", user, full_command) == 2) {

    // Store full_command in a separate variable for later use
    char primary_command[100]; 
    sscanf(full_command, "%s", primary_command);
        if (strcmp(primary_command, "USE") == 0) {
            char databaseName[100];
            sscanf(full_command, "USE DATABASE %s", databaseName);
            useDatabase(user, databaseName);
        } else if (strcmp(primary_command, "CREATE") == 0) {
            char argument[100];
            sscanf(full_command, "%*s %s", argument);

            if (strcmp(argument, "USER") == 0) {
                char user[100];
                char password[100];

                sscanf(full_command, "CREATE USER %s IDENTIFIED BY %s", user, password);
                addUser(user, password);
            } else if (strcmp(argument, "DATABASE") == 0) {
                char databaseName[100];
                sscanf(full_command, "CREATE DATABASE %s", databaseName);
                createDatabase(user, databaseName);
            }  else if (strcmp(argument, "TABLE") == 0) {
                char databaseName[100];
                char tableName[100];
                
                sscanf(full_command, "CREATE TABLE %s IN %s", tableName, databaseName);
                createTable(databaseName, tableName);
            }
        } else if (strcmp(primary_command, "GRANT") == 0) {
            char argument[100];
            sscanf(full_command, "%*s %s", argument);

            if (strcmp(argument, "PERMISSION") == 0) {
                char databaseName[100];
                char user[100];

                sscanf(full_command, "GRANT PERMISSION %s INTO %s", user, databaseName);
                grantPermission(user, databaseName);
            }
        } else if (strcmp(primary_command, "DROP") == 0) {
            char argument[100];
            sscanf(full_command, "%*s %s", argument);

            if (strcmp(argument, "DATABASE") == 0) {
                char databaseName[100];
                sscanf(full_command, "DROP DATABASE %s", databaseName);
                dropDatabase(databaseName);
            } else if (strcmp(argument, "TABLE") == 0) {
                char databaseName[100];
                char tableName[100];
                sscanf(full_command, "DROP TABLE %s FROM %s", tableName, databaseName);
                dropTable(databaseName, tableName);
            } else if (strcmp(argument, "COLUMN") == 0) {
                char databaseName[100];
                char tableName[100];
                char columnName[100];
                sscanf(full_command, "DROP COLUMN %s FROM %s.%s", columnName, tableName, databaseName);
                dropColumn(databaseName, tableName, columnName);
            }
        } else if (strcmp(primary_command, "INSERT") == 0) {
            insertIntoTable(user, full_command);
        } else if (strcmp(primary_command, "UPDATE") == 0) {
            if (strcmp(argument, "WHERE") == 0) {
                updateWithWhere(user, full_command);
            } else {
            updateTable(user, full_command);
            }
        } else if (strcmp(primary_command, "DELETE") == 0) {
            if (strcmp(argument, "WHERE") == 0) {
                deleteWithWhere(user, full_command);
            } else {
            updateTable(user, full_command);
            }
        } else if (strcmp(primary_command, "SELECT") == 0) {
            if (strcmp(argument, "WHERE") == 0) {
                selectWithWhere(user, full_command);
            } else {
            updateTable(user, full_command);
            }
        } else {
            printf("Unrecognized command\n");
            }
    }
}

int main(int argc, char const *argv[]) {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char command[COMMAND_SIZE];
    char full_command[COMMAND_SIZE];
    system("mkdir databases");
    system("mkdir databases/access");
    system("touch databases/access/users.txt");
    system("touch databases/access/permission.txt");

    // Create socket file descriptor
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Set socket options
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("Setsockopt failed");
        exit(EXIT_FAILURE);
    }

    // Define socket address and port
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Bind the socket to the specified port
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    // Listen for incoming connections
    if (listen(server_fd, 3) < 0) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

        printf("Server is waiting for incoming connections...\n");

    while (1) {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
            perror("Accept failed");
            exit(EXIT_FAILURE);
        }

        printf("New connection established\n");

        // Receive data from client and process it
        while (1) {
            memset(full_command, 0, COMMAND_SIZE);
            int valread;
            if ((valread = read(new_socket, full_command, COMMAND_SIZE)) == 0) {
                printf("Connection closed by client\n");
                break;
            }

            // Process the received full_command
            printf("Command received from client: %s\n", full_command);

            // Process the message received from the client
            processCommand(new_socket, full_command);
        }

        close(new_socket);
        printf("Client connection closed\n");
    }

    close(server_fd);
    return 0;
}
```
# Penjelasan Program
```

void log_command(const char *username, const char *command) {
    //menggunakan time saat ini
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_info);

    FILE *file = fopen("log.txt", "a");
    if (file == NULL) {
        perror("failed opening log.txt");
        return;
    }

    //menghilangkan ";"
    char clean_command[100];
    strcpy(clean_command, command);
    char *semicolon_plc = strchr(clean_command, ';');
    if (semicolon_plc != NULL) {
        *semicolon_plc = '\0';
    }

    //memasukan file ke log.txt
    fprintf(file, "%s:%s:%s\n", timestamp, username, clean_command);

    fclose(file);
}
```
- Fungsi ini digunakan untuk mencatat setiap perintah yang diterima oleh server ke dalam file log (`log.txt`). Setiap catatan log terdiri dari timestamp, nama pengguna, dan perintah yang dijalankan.
```
void grantPermission(char* user, char* databaseName) {
    FILE *file = fopen("databases/access/permission.txt", "a");
    if (file == NULL) {
        perror("Error opening permission file");
        return;
    }

    fprintf(file, "%s;%s\n", user, databaseName);
    fclose(file);
}
```
- Fungsi ini digunakan untuk memberikan izin kepada seorang pengguna (`user`) untuk menggunakan suatu basis data (`databaseName`). Izin tersebut dicatat dalam file `permission.txt` di dalam direktori `databases/access`. Setiap baris dalam file tersebut berisi nama pengguna dan nama basis data yang diizinkan, dipisahkan oleh tanda `;`.
```
bool hasPermission(char* user, char* databaseName) {
    if (strcmp(user, "root") == 0) {
        return true; // Grant all permissions to the root user
    }

    FILE *file = fopen("databases/access/permission.txt", "r");
    if (file == NULL) {
        perror("Error opening permission file");
        return false;
    }

    char line[COMMAND_SIZE];
    char db[100], usr[100];
    int found = false;

    while (fgets(line, sizeof(line), file)) {
        if (sscanf(line, "%[^;];%[^\n]", usr, db) == 2) {
            if (strcmp(usr, user) == 0 && strcmp(db, databaseName) == 0) {
                found = true;
                break;
            }
        }
    }

        fclose(file);
    if(found == true) {
        return true;
    } else {
        return false;
    }
}

```
- Fungsi ini memeriksa apakah seorang pengguna (`user`) memiliki izin untuk menggunakan suatu basis data (`databaseName`). Jika pengguna adalah "root," maka fungsi mengembalikan `true` karena "root" dianggap memiliki izin untuk segala hal. Jika tidak, fungsi membaca file `permission.txt` dan mencari apakah ada catatan izin yang sesuai.
``` 
void useDatabase(char* user, char* databaseName) {
    if (hasPermission(user, databaseName)) {
        printf("User %s is using database %s\n", user, databaseName);
    } else {
        printf("User %s does not have permission to use database %s\n", user, databaseName);
    }
}
```
- Fungsi ini memberi informasi apakah seorang pengguna memiliki izin untuk menggunakan suatu basis data atau tidak. Fungsi ini memanggil `hasPermission` dan mencetak pesan sesuai dengan hasilnya.
```
void addUser(char* user, char* password) {
    FILE *file = fopen("databases/access/users.txt", "a");
    if (file == NULL) {
        perror("Error opening users file");
        return;
    }

    fprintf(file, "%s;%s\n", user, password);
    fclose(file);
}
```
- Fungsi ini digunakan untuk menambahkan pengguna baru ke dalam file `users.txt` di dalam direktori `databases/access`. Setiap baris dalam file tersebut berisi nama pengguna dan kata sandi pengguna yang baru ditambahkan, dipisahkan oleh tanda `;`.
```
void createDatabase(char* user, char* databaseName){
    char command[COMMAND_SIZE];
    sprintf(command, "mkdir databases/%s", databaseName);

    int task = system(command);

    if (task == 1){
        printf(" Failure at creating %s database", databaseName);
    } else {
        printf("Success at creating %s database", databaseName);

        grantPermission(user, databaseName);
    }
}
```
- Fungsi ini digunakan untuk membuat sebuah basis data baru dengan nama tertentu. Fungsi ini membuat direktori baru dalam folder "databases" dan mencetak pesan berhasil atau gagal. Jika pembuatan berhasil, fungsi juga memberikan izin kepada pengguna (`user`) untuk menggunakan basis data tersebut dengan memanggil fungsi `grantPermission`.
```
void createTable(char* databaseName, char* tableName) {
    char command[COMMAND_SIZE];
    sprintf(command, "touch databases/%s/%s.txt", databaseName, tableName);
    int task = system(command);

    if (task != 0) {
        printf("Failed to create %s table in %s database\n", tableName, databaseName);
    } else {
        printf("Created %s table in %s database\n", tableName, databaseName);
    }
}
```
- Fungsi ini digunakan untuk membuat tabel baru dalam suatu basis data. Fungsi ini membuat file teks baru dengan ekstensi ".txt" di dalam folder basis data tersebut.
```
void dropColumn(char* databaseName, char* tableName, char* columnName) {
    char command[COMMAND_SIZE];
    sprintf(command, "sed -i '' '/%s/d' databases/%s/%s.txt", columnName, databaseName, tableName);
    int task = system(command);

    if (task != 0) {
        printf("Failed to drop %s column from %s table in %s database\n", columnName, tableName, databaseName);
    } else {
        printf("Dropped %s column from %s table in %s database\n", columnName, tableName, databaseName);
    }
}
```
- Fungsi ini digunakan untuk menghapus kolom tertentu dari sebuah tabel dalam suatu basis data. Fungsi ini menggunakan perintah `sed` untuk menghapus baris yang mengandung kolom tersebut dari file tabel.
```
void dropTable(char* databaseName, char* tableName) {
    char command[COMMAND_SIZE];
    sprintf(command, "rm -rf databases/%s/%s.txt", databaseName, tableName);
    int task = system(command);

    if (task != 0) {
        printf("Failed to delete %s table from %s database\n", tableName, databaseName);
    } else {
        printf("Deleted %s table from %s database\n", tableName, databaseName);
    }
}
```
- Fungsi ini digunakan untuk menghapus sebuah tabel dari suatu basis data. Fungsi ini menggunakan perintah `rm -rf` untuk menghapus file tabel.
```
void dropDatabase(char* databaseName) {
    char command[COMMAND_SIZE];
    sprintf(command, "rm -rf databases/%s", databaseName);
    int task = system(command);

    if (task != 0) {
        printf("Failed to delete %s database\n", databaseName);
    } else {
        printf("Deleted %s database\n", databaseName);
    }
}
```
- Fungsi ini digunakan untuk menghapus suatu basis data beserta semua tabelnya. Fungsi ini menggunakan perintah `rm -rf` untuk menghapus seluruh direktori basis data.
```
void insertData(char* databaseName, char* tableName, char* data) {
    char filePath[COMMAND_SIZE];
    sprintf(filePath, "databases/%s/%s.txt", databaseName, tableName);

    FILE *file = fopen(filePath, "a");
    if (file == NULL) {
        printf("Error opening table file\n");
        return;
    }

    fprintf(file, "%s\n", data);
    fclose(file);

    printf("Data inserted into %s table in %s database\n", tableName, databaseName);
}
```
- Fungsi ini digunakan untuk memasukkan data baru ke dalam sebuah tabel di suatu basis data. Data dimasukkan ke dalam file tabel dengan ekstensi ".txt".
```
void insertIntoTable(char* user, char* message) {
    char tableName[100];
    char values[COMMAND_SIZE];
    sscanf(message, "INSERT INTO %s %[^\n]", tableName, values);

    char databaseName[100]; 
    if (hasPermission(user, databaseName)) {
        insertData(databaseName, tableName, values);
    } else {
        printf("User %s does not have permission to insert into table %s in database %s\n", user, tableName, databaseName);
    }
}
```
- Fungsi ini memproses perintah `INSERT INTO` dan memanggil fungsi `insertData` jika pengguna memiliki izin untuk menulis ke dalam tabel tersebut.
```
void updateTable(char* user, char* message) {
    char tableName[100];
    char columnName[100];
    char value[COMMAND_SIZE];
    
    if (sscanf(message, "UPDATE %s SET %[^=]=%[^;];", tableName, columnName, value) == 3) {
        char databaseName[100]; 
        if (hasPermission(user, databaseName)) {
            // Logic to update the table
            char command[COMMAND_SIZE];
            sprintf(command, "sed -i '' 's/^%s=[^\n]*/%s=%s/' databases/%s/%s.txt", columnName, columnName, value, databaseName, tableName);
            
            int task = system(command);
            
            if (task != 0) {
                printf("Failed to update %s table in %s database\n", tableName, databaseName);
            } else {
                printf("Updated %s table in %s database\n", tableName, databaseName);
            }
        } else {
            printf("User %s does not have permission to update table %s in database %s\n", user, tableName, databaseName);
        }
    } else {
        printf("Invalid UPDATE command format\n");
    }
}
```
- Fungsi ini memproses perintah `UPDATE` untuk mengubah nilai dalam sebuah kolom pada suatu tabel. Fungsi ini menggunakan perintah `sed `untuk mengganti nilai dalam file tabel.
```
void updateWithWhere(char* user, char* message) {
    char tableName[100];
    char columnName[100];
    char value[COMMAND_SIZE];
    
    // Extracting data from the command
    sscanf(message, "UPDATE %s SET %[^=]=%[^;];", tableName, columnName, value);

    char databaseName[100]; // Define and assign the appropriate database name
    
    if (hasPermission(user, databaseName)) {
        // Open the file corresponding to the table for reading and writing
        char filePath[COMMAND_SIZE];
        sprintf(filePath, "databases/%s/%s.txt", databaseName, tableName);
        FILE* file = fopen(filePath, "r+");
        
        if (file == NULL) {
            printf("Table %s does not exist in database %s\n", tableName, databaseName);
            return;
        }

        char line[COMMAND_SIZE];
        char updatedLine[COMMAND_SIZE];
        while (fgets(line, sizeof(line), file)) {
            char *token = strtok(line, ",");
            while (token != NULL) {
                if (strstr(token, columnName) != NULL) {
                    // Assuming the condition column is found, update the line
                    if (strstr(token, value) != NULL) {
                        strcpy(updatedLine, line);
                        break;
                    }
                }
                token = strtok(NULL, ",");
            }
        }

        // Move the file pointer to the line where the update is required and write the updated line
        fseek(file, -strlen(updatedLine), SEEK_CUR);
        fprintf(file, "%s", updatedLine);
        
        fclose(file);
    } else {
        printf("User %s does not have permission to update table %s in database %s\n", user, tableName, databaseName);
    }
}
```
- Fungsi ini mirip dengan `updateTable`, namun memiliki kemampuan untuk memperbarui nilai berdasarkan kondisi tertentu (dengan menggunakan `WHERE`). Fungsi ini membuka file tabel, mencari dan memperbarui nilai yang sesuai.
```
void deleteFromTable(char* user, char* message) {
    char tableName[100];
    
    if (sscanf(message, "DELETE FROM %s;", tableName) == 1) {
        char databaseName[100]; 
        if (hasPermission(user, databaseName)) {
            // Logic to delete data from the table
            char command[COMMAND_SIZE];
            sprintf(command, "echo '' > databases/%s/%s.txt", databaseName, tableName);
            
            int task = system(command);
            
            if (task != 0) {
                printf("Failed to delete data from %s table in %s database\n", tableName, databaseName);
            } else {
                printf("Deleted data from %s table in %s database\n", tableName, databaseName);
            }
        } else {
            printf("User %s does not have permission to delete from table %s in database %s\n", user, tableName, databaseName);
        }
    } else {
        printf("Invalid DELETE command format\n");
    }
}
```
- Fungsi ini mengimplementasikan operasi penghapusan data dari tabel tanpa menggunakan kondisi `WHERE`. Perintah SQL `DELETE FROM` digunakan untuk menghapus semua data dari tabel. Fungsi ini mengekstrak nama tabel dari perintah dan memeriksa izin pengguna sebelum menjalankan operasi penghapusan. Jika pengguna memiliki izin, fungsi menggunakan perintah sistem (`system()`) untuk menjalankan perintah shell dan membersihkan isi file tabel menggunakan perintah `echo '' > file_tabel`.
```
void deleteWithWhere(char* user, char* message) {
    char tableName[100];
    char columnName[100];
    char value[COMMAND_SIZE];

    // Extracting data from the command
    sscanf(message, "DELETE FROM %s WHERE %[^=]=%[^;];", tableName, columnName, value);

    char databaseName[100]; // Define and assign the appropriate database name
    
    if (hasPermission(user, databaseName)) {
        // Open the file corresponding to the table for reading and writing
        char filePath[COMMAND_SIZE];
        sprintf(filePath, "databases/%s/%s.txt", databaseName, tableName);
        FILE* file = fopen(filePath, "r+");
        
        if (file == NULL) {
            printf("Table %s does not exist in database %s\n", tableName, databaseName);
            return;
        }

        char line[COMMAND_SIZE];
        char updatedLine[COMMAND_SIZE];
        while (fgets(line, sizeof(line), file)) {
            char *token = strtok(line, ",");
            while (token != NULL) {
                if (strstr(token, columnName) != NULL && strstr(token, value) != NULL) {
                    // Assuming the condition column is found, do not write the line
                    break;
                }
                token = strtok(NULL, ",");
            }
        }

        fclose(file);
    } else {
        printf("User %s does not have permission to delete from table %s in database %s\n", user, tableName, databaseName);
    }
}
```
- Fungsi ini mengimplementasikan operasi penghapusan data dari tabel dengan menggunakan kondisi `WHERE`. Perintah SQL `DELETE FROM` dengan klausa `WHERE` digunakan untuk menghapus data dari tabel sesuai dengan kondisi yang diberikan. Fungsi ini mengekstrak nama tabel, nama kolom, dan nilai dari perintah. Fungsi membuka file tabel, membaca setiap baris, dan mengabaikan baris yang memenuhi kondisi yang diberikan. Fungsi ini juga memeriksa izin pengguna sebelum menjalankan operasi penghapusan.
```
void selectFromTable(char* user, char* message) {
    char columns[COMMAND_SIZE];
    char tableName[100];
    sscanf(message, "SELECT %[^ ] FROM %s", columns, tableName);

    char databaseName[100]; // Define and assign the appropriate database name
    
    if (hasPermission(user, databaseName)) {
        // Open the file corresponding to the table for reading
        char filePath[COMMAND_SIZE];
        sprintf(filePath, "databases/%s/%s.txt", databaseName, tableName);
        FILE* file = fopen(filePath, "r");
        
        if (file == NULL) {
            printf("Table %s does not exist in database %s\n", tableName, databaseName);
            return;
        }

        // Logic for retrieving and displaying data based on the columns
        char line[COMMAND_SIZE];
        while (fgets(line, sizeof(line), file)) {
            // Assuming data is stored with comma-separated values in each row
            char *token = strtok(line, ",");
            while (token != NULL) {
                // If the requested column matches, print the value
                if (strcmp(columns, "*") == 0 || strstr(columns, token) != NULL) {
                    printf("%s ", token);
                }
                token = strtok(NULL, ",");
            }
            printf("\n");
        }

        fclose(file);
    } else {
        printf("User %s does not have permission to select from table %s in database %s\n", user, tableName, databaseName);
    }
}
```
- Fungsi ini mengimplementasikan operasi seleksi data dari tabel tanpa menggunakan kondisi `WHERE`. Perintah SQL `SELECT` digunakan untuk mengambil data dari tabel. Fungsi ini mengekstrak nama kolom dan nama tabel dari perintah. Fungsi membuka file tabel, membaca setiap baris, dan mencetak nilai dari kolom yang diminta.
```
void selectWithWhere(char* user, char* message) {
    char columns[COMMAND_SIZE];
    char tableName[100];
    char columnName[100];
    char value[COMMAND_SIZE];

    // Extracting data from the command
    sscanf(message, "SELECT %[^ ] FROM %s WHERE %[^=]=%[^;];", columns, tableName, columnName, value);

    char databaseName[100]; // Define and assign the appropriate database name
    
    if (hasPermission(user, databaseName)) {
        // Open the file corresponding to the table for reading
        char filePath[COMMAND_SIZE];
        sprintf(filePath, "databases/%s/%s.txt", databaseName, tableName);
        FILE* file = fopen(filePath, "r");
        
        if (file == NULL) {
            printf("Table %s does not exist in database %s\n", tableName, databaseName);
            return;
        }

        char line[COMMAND_SIZE];
        while (fgets(line, sizeof(line), file)) {
            char *token = strtok(line, ",");
            while (token != NULL) {
                if (strstr(token, columnName) != NULL && strstr(token, value) != NULL) {
                    // Assuming data is stored with comma-separated values in each row
                    // Print the entire row if the condition is met
                    printf("%s", line);
                    break;
                }
                token = strtok(NULL, ",");
            }
        }

        fclose(file);
    } else {
        printf("User %s does not have permission to select from table %s in database %s\n", user, tableName, databaseName);
    }
}
```
- Fungsi ini mengimplementasikan operasi seleksi data dari tabel dengan menggunakan kondisi `WHERE`. Perintah SQL `SELECT` dengan klausa `WHERE` digunakan untuk mengambil data dari tabel sesuai dengan kondisi yang diberikan. Fungsi ini mengekstrak nama kolom, nama tabel, nama kolom yang menjadi kondisi, dan nilai dari perintah. Fungsi membuka file tabel, membaca setiap baris, dan mencetak nilai dari kolom yang diminta hanya untuk baris yang memenuhi kondisi yang diberikan.
```
void processCommand(int new_socket, char *message) {
    char argument[COMMAND_SIZE];
    char response[COMMAND_SIZE];
    // sprintf(response, "Server received the command: %s", message);
    send(new_socket, response, strlen(response), 0);

    char user[100];
    char full_command[COMMAND_SIZE];
    
    if (sscanf(message, "\"%99[^\"]\" %9999[^\n]", user, full_command) == 2) {

    // Store full_command in a separate variable for later use
    char primary_command[100]; 
    sscanf(full_command, "%s", primary_command);
        if (strcmp(primary_command, "USE") == 0) {
            char databaseName[100];
            sscanf(full_command, "USE DATABASE %s", databaseName);
            useDatabase(user, databaseName);
        } else if (strcmp(primary_command, "CREATE") == 0) {
            char argument[100];
            sscanf(full_command, "%*s %s", argument);

            if (strcmp(argument, "USER") == 0) {
                char user[100];
                char password[100];

                sscanf(full_command, "CREATE USER %s IDENTIFIED BY %s", user, password);
                addUser(user, password);
            } else if (strcmp(argument, "DATABASE") == 0) {
                char databaseName[100];
                sscanf(full_command, "CREATE DATABASE %s", databaseName);
                createDatabase(user, databaseName);
            }  else if (strcmp(argument, "TABLE") == 0) {
                char databaseName[100];
                char tableName[100];
                
                sscanf(full_command, "CREATE TABLE %s IN %s", tableName, databaseName);
                createTable(databaseName, tableName);
            }
        } else if (strcmp(primary_command, "GRANT") == 0) {
            char argument[100];
            sscanf(full_command, "%*s %s", argument);

            if (strcmp(argument, "PERMISSION") == 0) {
                char databaseName[100];
                char user[100];

                sscanf(full_command, "GRANT PERMISSION %s INTO %s", user, databaseName);
                grantPermission(user, databaseName);
            }
        } else if (strcmp(primary_command, "DROP") == 0) {
            char argument[100];
            sscanf(full_command, "%*s %s", argument);

            if (strcmp(argument, "DATABASE") == 0) {
                char databaseName[100];
                sscanf(full_command, "DROP DATABASE %s", databaseName);
                dropDatabase(databaseName);
            } else if (strcmp(argument, "TABLE") == 0) {
                char databaseName[100];
                char tableName[100];
                sscanf(full_command, "DROP TABLE %s FROM %s", tableName, databaseName);
                dropTable(databaseName, tableName);
            } else if (strcmp(argument, "COLUMN") == 0) {
                char databaseName[100];
                char tableName[100];
                char columnName[100];
                sscanf(full_command, "DROP COLUMN %s FROM %s.%s", columnName, tableName, databaseName);
                dropColumn(databaseName, tableName, columnName);
            }
        } else if (strcmp(primary_command, "INSERT") == 0) {
            insertIntoTable(user, full_command);
        } else if (strcmp(primary_command, "UPDATE") == 0) {
            if (strcmp(argument, "WHERE") == 0) {
                updateWithWhere(user, full_command);
            } else {
            updateTable(user, full_command);
            }
        } else if (strcmp(primary_command, "DELETE") == 0) {
            if (strcmp(argument, "WHERE") == 0) {
                deleteWithWhere(user, full_command);
            } else {
            updateTable(user, full_command);
            }
        } else if (strcmp(primary_command, "SELECT") == 0) {
            if (strcmp(argument, "WHERE") == 0) {
                selectWithWhere(user, full_command);
            } else {
            updateTable(user, full_command);
            }
        } else {
            printf("Unrecognized command\n");
            }
    }
}
```
- Fungsi ini dipanggil untuk memproses perintah yang diterima dari klien. Fungsi mengekstrak nama pengguna dan perintah lengkap dari pesan yang diterima. Perintah diterjemahkan dan fungsi yang sesuai dipanggil untuk menjalankan operasi basis data.
```

int main(int argc, char const *argv[]) {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char command[COMMAND_SIZE];
    char full_command[COMMAND_SIZE];
    system("mkdir databases");
    system("mkdir databases/access");
    system("touch databases/access/users.txt");
    system("touch databases/access/permission.txt");

    // Create socket file descriptor
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Set socket options
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("Setsockopt failed");
        exit(EXIT_FAILURE);
    }

    // Define socket address and port
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Bind the socket to the specified port
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    // Listen for incoming connections
    if (listen(server_fd, 3) < 0) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

        printf("Server is waiting for incoming connections...\n");

    while (1) {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
            perror("Accept failed");
            exit(EXIT_FAILURE);
        }

        printf("New connection established\n");

        // Receive data from client and process it
        while (1) {
            memset(full_command, 0, COMMAND_SIZE);
            int valread;
            if ((valread = read(new_socket, full_command, COMMAND_SIZE)) == 0) {
                printf("Connection closed by client\n");
                break;
            }

            // Process the received full_command
            printf("Command received from client: %s\n", full_command);

            // Process the message received from the client
            processCommand(new_socket, full_command);
        }

        close(new_socket);
        printf("Client connection closed\n");
    }

    close(server_fd);
    return 0;
}
```
- Inisialisasi Direktori dan File:
   - Membuat direktori `databases` dan `databases/access` menggunakan perintah sistem `system("mkdir databases")`.
   - Membuat file `users.txt` dan `permission.txt` di dalam direktori `databases/access` menggunakan perintah `system("touch databases/access/users.txt")` dan `system("touch databases/access/permission.txt")`.

- Inisialisasi Socket:
   - Membuat socket menggunakan `socket(AF_INET, SOCK_STREAM, 0)` untuk koneksi berbasis TCP/IP.
   - Mengatur opsi socket dengan `setsockopt` untuk mengizinkan penggunaan kembali alamat dan port dengan `SO_REUSEADDR` dan `SO_REUSEPORT`.
   - Mengisi struktur `address` dengan alamat dan port yang diinginkan.
   - Mengeksekusi fungsi `bind` untuk mengikat socket ke alamat dan port yang telah ditentukan.
   - Mengeksekusi fungsi `listen` untuk memulai mode listening pada socket dan menentukan jumlah maksimum koneksi yang diizinkan (dalam hal ini, 3).

- Menerima Koneksi:
   - Program memasuki loop utama untuk menerima koneksi dari klien.
   - Fungsi `accept` digunakan untuk menerima koneksi dari klien baru dan membuat socket baru (`new_socket`) untuk berkomunikasi dengan klien tersebut.

- Menerima dan Memproses Perintah dari Klien:
   - Setelah koneksi berhasil didirikan, program memasuki loop kedua untuk menerima dan memproses perintah dari klien.
   - Fungsi `read` digunakan untuk membaca perintah yang dikirimkan oleh klien dari socket.
   - Setelah membaca perintah, fungsi `processCommand` dipanggil untuk memproses perintah tersebut.

- Fungsi `processCommand`:
   - Fungsi ini memproses perintah yang diterima dari klien.
   - Mengekstrak nama pengguna dan perintah lengkap dari pesan yang diterima.
   - Menerjemahkan perintah dan memanggil fungsi yang sesuai untuk menjalankan operasi basis data.

- Menutup Koneksi:
   - Setelah selesai memproses perintah dari klien, socket klien ditutup menggunakan fungsi `close(new_socket)`.
   - Program kembali ke tahap menerima koneksi baru dan mengulangi proses.

- Catatan Penting:
   - Program ini bersifat sederhana dan belum sepenuhnya lengkap. Beberapa operasi seperti `useDatabase`, `addUser`, `grantPermission`, dan lainnya belum diimplementasikan.
   - Program ini menggunakan fungsi sistem (`system()`) untuk menjalankan perintah shell, yang dapat menyebabkan potensi masalah keamanan seperti injeksi shell.
   - Pengelolaan basis data sebenarnya memerlukan pendekatan yang lebih matang dalam hal keamanan, manajemen transaksi, dan kinerja.

# cron.sh 
```
#!/bin/bash

backupDirectory="backup_files/"
runtimeDirectory="../"
timestamp=$(date +\%Y-\%m-\%d_\%H:\%M:\%S)
zipFile="$timestamp-archive.zip"

createBackup() {
    local directory="$1"
    sudo ~/backup-scripts/dump_script "$(basename "$directory")" > "$backupDirectory/$(basename "$directory").backup"
}

copyLogToBackup() {
    cp "$runtimeDirectory/log.txt" "$backupDirectory"
}

clearOriginalLog() {
    echo -n > "$runtimeDirectory/log.txt"
}

createZip() {
    cd "$backupDirectory" && zip "$zipFile" *.backup log.txt
    find "$backupDirectory" -maxdepth 1 -type f ! -name "$zipFile" -delete
}

# Create backups for each sub-directory
for directory in "$backupDirectory"/*; do
    createBackup "$directory"
done

copyLogToBackup
clearOriginalLog
createZip
```
# Penjelasan Program 

1. **Variabel Global:**
   ```
   backupDirectory="backup_files/"
   runtimeDirectory="../"
   timestamp=$(date +\%Y-\%m-\%d_\%H:\%M:\%S)
   zipFile="$timestamp-archive.zip"
   ```
   - `backupDirectory`: Menyimpan nama direktori untuk menyimpan file-file backup.
   - `runtimeDirectory`: Menyimpan nama direktori untuk log dan file-file lainnya.
   - `timestamp`: Menyimpan timestamp saat ini dalam format yang digunakan untuk nama file backup.
   - `zipFile`: Menyimpan nama file zip arsip yang akan dibuat.

2. **Fungsi `createBackup`:**
   ```
   createBackup() {
       local directory="$1"
       sudo ~/backup-scripts/dump_script "$(basename "$directory")" > "$backupDirectory/$(basename "$directory").backup"
   }
   ```
   - Membuat backup dari suatu direktori.
   - Memanggil skrip `dump_script` (dengan sudo) untuk mendump isi direktori tersebut ke dalam file backup dengan format nama file: `namadirektori.backup`.

3. **Fungsi `copyLogToBackup`:**
   ```
   copyLogToBackup() {
       cp "$runtimeDirectory/log.txt" "$backupDirectory"
   }
   ```
   - Mencopy file log (`log.txt`) dari direktori runtime ke dalam direktori backup.

4. **Fungsi `clearOriginalLog`:**
   ```
   clearOriginalLog() {
       echo -n > "$runtimeDirectory/log.txt"
   }
   ```
   - Mengosongkan isi dari file log asli (`log.txt`).

5. **Fungsi `createZip`:**
   ```
   createZip() {
       cd "$backupDirectory" && zip "$zipFile" *.backup log.txt
       find "$backupDirectory" -maxdepth 1 -type f ! -name "$zipFile" -delete
   }
   ```
   - Pindah ke dalam direktori backup dan membuat arsip zip dari file-file backup dan file log.
   - Menghapus file-file backup yang tidak termasuk dalam arsip zip.

6. **Loop untuk Membuat Backup untuk Setiap Sub-direktori:**
   ```
   for directory in "$backupDirectory"/*; do
       createBackup "$directory"
   done
   ```
   - Melakukan iterasi untuk setiap sub-direktori di dalam direktori backup dan memanggil fungsi `createBackup` untuk membuat backup dari masing-masing sub-direktori.

7. **Panggilan Fungsi:**
   ```
   copyLogToBackup
   clearOriginalLog
   createZip
   ```
   - Memanggil fungsi `copyLogToBackup`, `clearOriginalLog`, dan `createZip` untuk menjalankan langkah-langkah backup dan pengarsipan.

# client_dump.c
```
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <getopt.h>

#define PORT 8080
#define COMMAND_SIZE 10000
#define FULL_COMMAND_SIZE 12002

void saveDataToDatabase(const char *user, const char *password, const char *database) {
    // Implementasi logika penyimpanan database
    // Contoh: Menyimpan data ke file atau melakukan akses ke database sesuai kebutuhan aplikasi
    FILE *databaseFile = fopen("../database/databases/data.txt", "a");
    if (databaseFile == NULL) {
        perror("Error opening database file");
        exit(EXIT_FAILURE);
    }

    fprintf(databaseFile, "User: %s, Password: %s, Database: %s\n", user, password, database);

    fclose(databaseFile);
}

int main(int argc, char const *argv[]) {
    char user[2000], password[2000], database[2000];
    struct sockaddr_in serv_addr;
    int sock = 0;
    int option;
    int u_flag = 0, p_flag = 0, d_flag = 0;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("\nSocket creation error");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        perror("\nInvalid address/Address not supported");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("\nConnection Failed");
        return -1;
    }

    uid_t user_id = getuid();

    if (user_id == 0) {
        while (1) {
            printf("Root user detected\n");
            // Handle root user logic here
        }
    } else {
        // Receiving input for user and password
        while ((option = getopt(argc, argv, "u:p:d:")) != -1) {
            switch (option) {
                case 'u':
                    strncpy(user, optarg, sizeof(user) - 1);
                    u_flag = 1;
                    break;
                case 'p':
                    strncpy(password, optarg, sizeof(password) - 1);
                    p_flag = 1;
                    break;
                case 'd':
                    strncpy(database, optarg, sizeof(database) - 1);
                    d_flag = 1;
                    break;
                default:
                    fprintf(stderr, "Invalid option\n");
                    exit(EXIT_FAILURE);
            }
        }

        // Check if -u, -p, and -d options are provided
        if (!(u_flag && p_flag && d_flag)) {
            fprintf(stderr, "Please provide username (-u), password (-p), and database (-d)\n");
            exit(EXIT_FAILURE);
        }

        printf("User: %s, Password: %s, Database: %s\n", user, password, database);

        FILE *file = fopen("../database/databases/access/users.txt", "r");
        int userFound = 0;

        if (file == NULL) {
            // File doesn't exist, create it and set permissions
            file = fopen("../database/databases/access/users.txt", "w");
            if (file == NULL) {
                perror("Error creating users.txt");
                exit(EXIT_FAILURE);
            }
            fclose(file);

            if (chmod("../database/databases/access/users.txt", 0666) == -1) {
                perror("Error setting permissions for users.txt");
                exit(EXIT_FAILURE);
            }
        } else {
            char line[100];
            while (fgets(line, sizeof(line), file)) {
                line[strcspn(line, "\n")] = '\0';
                if (strstr(line, user) != NULL && strstr(line, password) != NULL) {
                    userFound = 1;
                    break;
                }
            }
            fclose(file);
        }

        if (userFound == 0) {
            fprintf(stderr, "User not found or password is incorrect\n");
            exit(EXIT_FAILURE);
        } else {
            printf("User found\n");
            // Handle non-root user logic here (e.g., THE USE DATABASE METHOD)
            saveDataToDatabase(user, password, database);
        }
    }
    return 0;
}
```
# Penjelasan Program
1. **Fungsi `saveDataToDatabase`**
```
    void saveDataToDatabase(const char *user, const char *password, const char *database) {
        // Implementasi logika penyimpanan database
        // Contoh: Menyimpan data ke file atau melakukan akses ke database sesuai kebutuhan aplikasi
        FILE *databaseFile = fopen("../database/databases/data.txt", "a");
        if (databaseFile == NULL) {
            perror("Error opening database file");
            exit(EXIT_FAILURE);
        }

        fprintf(databaseFile, "User: %s, Password: %s, Database: %s\n", user, password, database);

        fclose(databaseFile);
    }
```
    - Mendefinisikan fungsi `saveDataToDatabase` yang bertugas menyimpan data user, password, dan database ke dalam suatu penyimpanan, misalnya file. Fungsi ini membuka file `data.txt` dalam mode "a" (append) dan menulis informasi tersebut ke dalamnya.

2. **Cek User Root dan Handling**
```
    uid_t user_id = getuid();
    if (user_id == 0) {
        while (1) {
            printf("Root user detected\n");
            // Handle root user logic here
        }
    } else {
```
    - Mendapatkan User ID menggunakan `getuid()` dan memeriksa apakah program dijalankan oleh root user (UID 0). Jika ya, maka program masuk ke dalam loop tak terbatas yang mencetak pesan "Root user detected". Pada bagian ini, seharusnya ada logika khusus untuk pengguna root, namun saat ini hanya ada pesan cetak.

3. **Penerimaan Input dari Command-Line**
```
    while ((option = getopt(argc, argv, "u:p:d:")) != -1) {
        switch (option) {
            case 'u':
                strncpy(user, optarg, sizeof(user) - 1);
                u_flag = 1;
                break;
            case 'p':
                strncpy(password, optarg, sizeof(password) - 1);
                p_flag = 1;
                break;
            case 'd':
                strncpy(database, optarg, sizeof(database) - 1);
                d_flag = 1;
                break;
            default:
                fprintf(stderr, "Invalid option\n");
                exit(EXIT_FAILURE);
        }
    }
```
    - Menggunakan `getopt` untuk mem-parsing opsi dari command-line, yaitu `-u` (username), `-p` (password), dan `-d` (database). Informasi yang diperoleh disalin ke dalam variabel `user`, `password`, dan `database`. Selain itu, flag `u_flag`, `p_flag`, dan `d_flag` diatur untuk menandakan bahwa opsi tersebut telah diberikan.

4. **Pengecekan Opsi Command-Line**
```
    if (!(u_flag && p_flag && d_flag)) {
        fprintf(stderr, "Please provide username (-u), password (-p), and database (-d)\n");
        exit(EXIT_FAILURE);
    }
```
    - Memeriksa apakah semua opsi `-u`, `-p`, dan `-d` telah diberikan. Jika tidak, program mencetak pesan kesalahan dan keluar.

5. **Cetak Informasi User, Password, dan Database**
```
    printf("User: %s, Password: %s, Database: %s\n", user, password, database);
```
    - Mencetak informasi user, password, dan database yang telah diperoleh dari command-line.

6. **Pembukaan dan Pemeriksaan File Users.txt**
```
    FILE *file = fopen("../database/databases/access/users.txt", "r");
    int userFound = 0;

    if (file == NULL) {
        // File doesn't exist, create it and set permissions
        file = fopen("../database/databases/access/users.txt", "w");
        if (file == NULL) {
            perror("Error creating users.txt");
            exit(EXIT_FAILURE);
        }
        fclose(file);

        if (chmod("../database/databases/access/users.txt", 0666) == -1) {
            perror("Error setting permissions for users.txt");
            exit(EXIT_FAILURE);
        }
    } else {
        // ...
    }
```
    - Membuka file `users.txt` untuk membaca. Jika file tidak ada, maka file akan dibuat dan diberi izin baca-tulis untuk semua (`0666`).

7. **Autentikasi dari File Users.txt**
```
    char line[100];
    while (fgets(line, sizeof(line), file)) {
        line[strcspn(line, "\n")] = '\0';
        if (strstr(line, user) != NULL && strstr(line, password) != NULL) {
            userFound = 1;
            break;
        }
    }
    fclose(file);
```
    - Membaca setiap baris di dalam file `users.txt`. Jika ditemukan kombinasi username dan password yang sesuai, maka `userFound` diatur menjadi 1.

8. **Penanganan User Tidak Ditemukan**
```
    if (userFound == 0) {
        fprintf(stderr, "User not found or password is incorrect\n");
        exit(EXIT_FAILURE);
    } else {
        printf("User found\n");
        // Handle non-root user logic here (e.g., THE USE DATABASE METHOD)
        saveDataToDatabase(user, password, database);
    }
```
    - Jika `userFound` masih 0, artinya username tidak ditemukan atau password salah, maka program mencetak pesan kesalahan dan keluar. Jika ditemukan, program mencetak "User found" dan memanggil fungsi `saveDataToDatabase` untuk menyimpan data ke dalam database (dalam hal ini ke dalam file).